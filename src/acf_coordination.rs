use crate::util::intersection;
use chemfiles as chfl;
use chfl_tools::datasets::WOADataset;
use chfl_tools::filters::ResidueFilter;
use chfl_tools::ranges::LogRange;
use log::{debug, info};
use rayon::prelude::*;

const LOG_STEPS: usize = 1000;

fn acf_frames<T: std::marker::Copy + std::cmp::Eq>(
    framedata_a: &[Vec<T>],
    framedata_b: &[Vec<T>],
) -> (usize, usize) {
    let mut associated = 0;
    let mut dissociated = 0;
    framedata_a
        .iter()
        .zip(framedata_b.iter())
        .filter(|(vec_a, _)| !vec_a.is_empty())
        .for_each(|(vec_a, vec_b)| {
            let coord: Vec<_> = intersection(vec_a, vec_b).iter().copied().collect();
            associated += coord.len();
            dissociated += vec_a.len() - coord.len();
        });
    (associated, dissociated)
}

fn create_coord_timeseries(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    step_size: usize,
    acf_residues: bool,
) -> Vec<Vec<Vec<Vec<u64>>>> {
    let mut all_atom_idx = probe.to_owned();
    ligands.iter().for_each(|&lig| all_atom_idx.extend(lig));
    let n_ligands = ligands.len();

    let mut coord_timeseries = vec![Vec::new(); n_ligands];

    let get_coordinating_idx: Box<dyn Fn(&chfl::Frame, &[_], &[_]) -> Vec<_>> = if acf_residues {
        Box::new(|frame, result, lig| {
            frame.unique_residues(intersection(result, &lig).into_iter().copied())
        })
    } else {
        Box::new(|_, result, lig| {
            intersection(result, &lig)
                .into_iter()
                .map(|&x| x as u64)
                .collect()
        })
    };

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();
        let mut timeseries_entries = vec![Vec::with_capacity(probe.len()); n_ligands];

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        probe.iter().for_each(|&probe_idx| {
            let result = nsgrid.get_neighbours(probe_idx).unwrap();
            timeseries_entries
                .iter_mut()
                .zip(ligands.iter())
                .for_each(|(tse, lig)| tse.push(get_coordinating_idx(&frame, &result, lig)));
        });

        coord_timeseries
            .iter_mut()
            .zip(timeseries_entries.into_iter())
            .for_each(|(cts, tse)| cts.push(tse));
    }

    coord_timeseries
}

fn calc_acf_coord<T: std::marker::Copy + std::cmp::Eq>(
    coord_timeseries: &[Vec<Vec<T>>],
) -> Vec<(usize, f64, f64, usize)>
where
    T: std::marker::Sync,
{
    let cts_len = coord_timeseries.len();

    let acf_data: Vec<_> = LogRange::new(0, cts_len - 1, 2, true)
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(|step| {
            info!("step: {}", step);
            let mut step_data = WOADataset::new();
            for i in 0..(cts_len - step) {
                let j = i + step;
                let (associated, dissociated) =
                    acf_frames(&coord_timeseries[i], &coord_timeseries[j]);
                (0..associated).for_each(|_| step_data.update(1.0));
                (0..dissociated).for_each(|_| step_data.update(0.0));
            }
            (
                step,
                step_data.calc_mean().unwrap_or(0.0),
                step_data.calc_std().unwrap_or(0.0),
                step_data.len(),
            )
        })
        .collect();

    acf_data
}

pub fn acf_coord(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    step_size: Option<usize>,
    residues: bool,
) -> Vec<Vec<(usize, f64, f64, usize)>> {
    let step_size = step_size.unwrap_or(1);

    let coord_timeseries =
        create_coord_timeseries(traj, probe, ligands, radius, step_size, residues);

    coord_timeseries
        .par_iter()
        .map(|cts| calc_acf_coord(&cts))
        .collect()
}
