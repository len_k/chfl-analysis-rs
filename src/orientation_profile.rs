use chemfiles as chfl;
use chfl_tools::centers::Center;
use chfl_tools::datasets::BinnedData;
use log::{debug, info};

const LOG_STEPS: usize = 1000;

pub fn orient_profile(
    traj: &mut chfl::Trajectory,
    atom_groups: &[[Vec<usize>; 2]],
    n_bins: usize,
    step_size: Option<usize>,
) -> BinnedData {
    let step_size = step_size.unwrap_or(1);

    let mut binned_data = BinnedData::new(n_bins, 0.0, 1.0);

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let height = frame.cell().lengths()[2];
        atom_groups.iter().for_each(|ag| {
            let center1 = frame.center_atoms(&ag[0], None, false);
            let center2 = frame.center_atoms(&ag[1], None, false);
            let ori = [
                center2[0] - center1[0],
                center2[1] - center1[1],
                center2[2] - center1[2],
            ];
            let length = (ori[0].powi(2) + ori[1].powi(2) + ori[2].powi(2)).sqrt();
            let mut z = (center1[2] / height) % 1.0;
            if z < 0.0 {
                z += 1.0;
            }
            let angle = (ori[2] / length).acos();
            binned_data.insert(z, angle.to_degrees());
        });
    }

    binned_data
}
