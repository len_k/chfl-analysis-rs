use crate::util::intersection;
use chemfiles as chfl;
use chfl_tools::filters::ResidueFilter;
use log::{debug, info};
use rayon::prelude::*;

const LOG_STEPS: usize = 1000;

fn unique_count<T: std::marker::Copy + std::cmp::Ord>(arr: &[T]) -> Vec<(T, usize)> {
    let mut last = None;
    let mut result: Vec<(_, _)> = Vec::new();
    let mut sorted_array: Vec<_> = arr.iter().copied().collect();
    sorted_array.sort();
    sorted_array.into_iter().for_each(|val| {
        if let Some(last) = last {
            if last == val {
                result.last_mut().unwrap().1 += 1;
            } else {
                result.push((val, 1));
            }
        } else {
            result.push((val, 1));
        }
        last = Some(val);
    });
    result
}

pub fn coord_profile(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    n_bins: usize,
    step_size: Option<usize>,
    residues: bool,
) -> Vec<Vec<Vec<(usize, usize)>>> {
    let step_size = step_size.unwrap_or(1);

    let mut all_atom_idx = probe.to_owned();
    ligands.iter().for_each(|&lig| all_atom_idx.extend(lig));

    let mut coord_binned = vec![vec![Vec::new(); n_bins]; ligands.len()];

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let height = frame.cell().lengths()[2];
        let positions = frame.positions();
        #[allow(
            clippy::cast_possible_truncation,
            clippy::cast_precision_loss,
            clippy::cast_sign_loss
        )]
        let frame_bins: Vec<_> = probe
            .iter()
            .map(|&i| positions[i])
            .map(|pos| (((pos[2] / height) % 1.0) * n_bins as f64).floor())
            .map(|bin| {
                if bin < 0.0 {
                    (bin + n_bins as f64) as usize
                } else {
                    bin as usize
                }
            })
            .collect();

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        probe
            .iter()
            .zip(frame_bins.into_iter())
            .for_each(|(&probe_idx, bin)| {
                let result = nsgrid.get_neighbours(probe_idx).unwrap();
                let ts_coord: Vec<_> = if residues {
                    ligands
                        .iter()
                        .map(|lig| {
                            frame
                                .unique_residues(intersection(&result, &lig).into_iter().copied())
                                .len()
                        })
                        .collect()
                } else {
                    ligands
                        .par_iter()
                        .map(|lig| intersection(&result, &lig).into_iter().len())
                        .collect()
                };
                coord_binned
                    .iter_mut()
                    .zip(ts_coord.into_iter())
                    .for_each(|(ca, tsc)| {
                        ca[bin].push(tsc);
                    });
            });
    }

    coord_binned
        .into_iter()
        .map(|binned_coord_lig| {
            binned_coord_lig
                .into_iter()
                .map(|coord_lig| unique_count(&coord_lig))
                .collect()
        })
        .collect()
}
