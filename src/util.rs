use clap::{App, Arg};
use simplelog::{Config, ConfigBuilder};

#[must_use]
pub fn get_cli_args() -> (Option<String>, Option<String>, Option<String>) {
    let args_matches = App::new("ACF Coordination")
        .arg(
            Arg::with_name("traj")
                .short("f")
                .value_name("FILE")
                .help("Trajectory [<.xtc/.trr/...>]")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("topol")
                .short("s")
                .value_name("FILE")
                .help("Topology [<.gro/.tpr/...>]")
                .required(false)
                .takes_value(true),
        )
        .arg(
            Arg::with_name("out_path")
                .short("o")
                .value_name("PATH")
                .help("Output Path")
                .required(false)
                .takes_value(true),
        )
        .get_matches();

    (
        args_matches
            .value_of("traj")
            .map(std::string::ToString::to_string),
        args_matches
            .value_of("topol")
            .map(std::string::ToString::to_string),
        args_matches
            .value_of("out_path")
            .map(std::string::ToString::to_string),
    )
}

pub fn intersection<'a, T: std::marker::Copy + std::cmp::Eq>(a: &'a [T], b: &'a [T]) -> Vec<&'a T> {
    a.iter().filter(|e| b.contains(e)).collect()
}

pub trait Array3Ops {
    fn add(&self, other: &Self) -> Self;
    fn neg(&self, other: &Self) -> Self;
}

impl Array3Ops for [f64; 3] {
    fn add(&self, other: &Self) -> Self {
        [self[0] + other[0], self[1] + other[1], self[2] + other[2]]
    }

    fn neg(&self, other: &Self) -> Self {
        [self[0] - other[0], self[1] - other[1], self[2] - other[2]]
    }
}

#[derive(Clone, Copy)]
pub enum Direction {
    X,
    Y,
    Z,
}

#[must_use]
pub fn log_format_date_time() -> Config {
    ConfigBuilder::new().set_time_format_str("%F %T").build()
}
