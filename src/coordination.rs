use crate::util::intersection;
use chemfiles as chfl;
use chfl_tools::datasets::WOADataset;
use chfl_tools::filters::ResidueFilter;
use itertools::Itertools;
use log::{debug, info};
use rayon::prelude::*;
use std::collections::HashMap;

const LOG_STEPS: usize = 1000;

fn update_counter_with_ts(
    coord_counter: &mut Vec<HashMap<usize, WOADataset>>,
    coord_counter_ts: Vec<HashMap<usize, usize>>,
) {
    coord_counter_ts
        .into_iter()
        .enumerate()
        .for_each(|(lig_idx, counter_hm)| {
            counter_hm.into_iter().for_each(|(cn, count)| {
                let cn_ds = coord_counter[lig_idx].get_mut(&cn);
                #[allow(clippy::cast_precision_loss)]
                if let Some(cn_ds) = cn_ds {
                    cn_ds.update(count as f64);
                } else {
                    let mut ds = WOADataset::new();
                    ds.update(count as f64);
                    coord_counter[lig_idx].insert(cn, ds);
                }
            });
        });
}

fn convert_to_result(
    coord_counter: Vec<HashMap<usize, WOADataset>>,
    nsteps: usize,
    nprobes: usize,
) -> Vec<Vec<(usize, f64, f64, f64)>> {
    let mut coord_result = vec![];
    coord_counter.into_iter().for_each(|coord_counter_lig| {
        #[allow(clippy::cast_precision_loss)]
        let mut coord_lig: Vec<_> = coord_counter_lig
            .into_iter()
            .map(|(a, b)| {
                (
                    a,
                    b.calc_mean().unwrap() / nprobes as f64,
                    b.calc_std().unwrap() / nprobes as f64,
                    b.len() as f64 / nsteps as f64,
                )
            })
            .collect();
        coord_lig.par_sort_by(|a, b| a.0.cmp(&b.0));
        coord_result.push(coord_lig);
    });
    coord_result
}

fn update_ts_counter(
    coord_counter_ts: &mut Vec<HashMap<usize, usize>>,
    search_result: &[usize],
    frame: &chfl::Frame,
    ligands: &[&[usize]],
    residues: bool,
) {
    let coord_nums: Vec<_> = if residues {
        ligands
            .iter()
            .map(|lig| {
                frame
                    .unique_residues(intersection(search_result, &lig).into_iter().copied())
                    .len()
            })
            .collect()
    } else {
        ligands
            .par_iter()
            .map(|lig| intersection(search_result, &lig).into_iter().len())
            .collect()
    };
    coord_nums
        .into_iter()
        .enumerate()
        .for_each(|(lig_idx, cn)| {
            let prev_val = coord_counter_ts[lig_idx].get_mut(&cn);
            if let Some(prev_val) = prev_val {
                *prev_val += 1;
            } else {
                coord_counter_ts[lig_idx].insert(cn, 1);
            }
        });
}

pub fn coord(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    step_size: Option<usize>,
    residues: bool,
) -> Vec<Vec<(usize, f64, f64, f64)>> {
    let step_size = step_size.unwrap_or(1);

    let all_atom_idx = {
        let mut all_atom_idx = probe.to_owned();
        ligands.iter().for_each(|&lig| all_atom_idx.extend(lig));
        all_atom_idx
    };

    let mut coord_counter = vec![HashMap::<_, WOADataset>::new(); ligands.len()];

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let mut coord_counter_ts = vec![HashMap::new(); ligands.len()];

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        probe.iter().for_each(|&probe_idx| {
            let result = nsgrid.get_neighbours(probe_idx).unwrap();
            update_ts_counter(&mut coord_counter_ts, &result, &frame, ligands, residues);
        });

        update_counter_with_ts(&mut coord_counter, coord_counter_ts);
    }

    let nsteps = traj.nsteps().unwrap() / step_size + 1;
    convert_to_result(coord_counter, nsteps, probe.len())
}

pub fn coord_res(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    step_size: Option<usize>,
    residues: bool,
) -> Vec<Vec<(usize, f64, f64, f64)>> {
    let step_size = step_size.unwrap_or(1);

    let all_atom_idx = {
        let mut all_atom_idx = probe.to_owned();
        ligands.iter().for_each(|&lig| all_atom_idx.extend(lig));
        all_atom_idx
    };

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let mut probe_res_map = HashMap::<_, Vec<_>>::new();
    for probe_idx in probe {
        let res_id = frame
            .topology()
            .residue_for_atom(*probe_idx)
            .unwrap()
            .id()
            .unwrap();
        let idx_vec = probe_res_map.get_mut(&res_id);
        if let Some(idx_vec) = idx_vec {
            idx_vec.push(probe_idx);
        } else {
            probe_res_map.insert(res_id, vec![probe_idx]);
        }
    }

    let mut coord_counter = vec![HashMap::<_, WOADataset>::new(); ligands.len()];

    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let mut coord_counter_ts = vec![HashMap::new(); ligands.len()];

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        probe_res_map.values().for_each(|probe_res| {
            let result: Vec<_> = {
                let mut result = Vec::new();
                probe_res.iter().for_each(|probe_idx| {
                    result.extend(nsgrid.get_neighbours(**probe_idx).unwrap());
                });
                result.into_iter().unique().collect()
            };
            update_ts_counter(&mut coord_counter_ts, &result, &frame, ligands, residues);
        });

        update_counter_with_ts(&mut coord_counter, coord_counter_ts);
    }

    let nsteps = traj.nsteps().unwrap() / step_size + 1;
    convert_to_result(coord_counter, nsteps, probe_res_map.len())
}
