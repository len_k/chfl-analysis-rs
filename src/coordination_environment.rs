use crate::util::intersection;
use chemfiles as chfl;
use chfl_tools::filters::ResidueFilter;
use log::{debug, info};
use rayon::prelude::*;
use std::collections::HashMap;

const LOG_STEPS: usize = 1000;

pub fn coord_env(
    traj: &mut chfl::Trajectory,
    probe: &[usize],
    ligands: &[&[usize]],
    radius: f64,
    step_size: Option<usize>,
    residues: bool,
) -> Vec<(usize, Vec<usize>)> {
    let step_size = step_size.unwrap_or(1);

    let mut all_atom_idx = probe.to_owned();
    ligands.iter().for_each(|&lig| all_atom_idx.extend(lig));

    let mut env_counter = HashMap::new();

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        probe.iter().for_each(|&probe_idx| {
            let result = nsgrid.get_neighbours(probe_idx).unwrap();
            let mut tse: Vec<_> = ligands
                .par_iter()
                .map(|lig| intersection(&result, &lig).into_iter().len())
                .collect();
            if residues {
                let tse_res: Vec<_> = ligands
                    .iter()
                    .map(|lig| {
                        frame
                            .unique_residues(intersection(&result, &lig).into_iter().copied())
                            .len()
                    })
                    .collect();
                tse.extend(tse_res);
            }
            let prev_val = env_counter.get_mut(&tse);
            if let Some(prev_val) = prev_val {
                *prev_val += 1;
            } else {
                env_counter.insert(tse, 1);
            }
        });
    }

    let mut env_result: Vec<_> = env_counter.into_iter().map(|(a, b)| (b, a)).collect();
    env_result.par_sort_by(|a, b| a.0.cmp(&b.0).reverse());
    env_result
}
