use crate::util::intersection;
use chemfiles as chfl;
use chfl_tools::datasets::Histogram;
use log::{debug, info};

const LOG_STEPS: usize = 1000;

pub fn coord_angle_dist(
    traj: &mut chfl::Trajectory,
    angles: &[&[usize]; 3],
    radius: f64,
    n_bins: usize,
    step_size: Option<usize>,
) -> Histogram<usize> {
    let step_size = step_size.unwrap_or(1);
    let mut all_atom_idx = angles[0].to_owned();
    all_atom_idx.extend(angles[1]);
    all_atom_idx.extend(angles[2]);

    let mut hist = Histogram::new(n_bins, 0.0, 180.0);

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let nsgrid = nsgrid::NSGrid::with_selection(&frame, radius, all_atom_idx.clone()).unwrap();
        angles[1].iter().for_each(|&probe_idx| {
            let result = nsgrid.get_neighbours(probe_idx).unwrap();
            let lig1 = intersection(&result, angles[0]);
            let lig2 = intersection(&result, angles[2]);
            lig1.into_iter()
                .filter(|&&lig1_idx| lig1_idx != probe_idx)
                .for_each(|&lig1_idx| {
                    lig2.iter()
                        .filter(|&&&lig2_idx| lig2_idx != probe_idx)
                        .filter(|&&&lig2_idx| lig2_idx != lig1_idx)
                        .for_each(|&&lig2_idx| {
                            let angle = frame.angle(lig1_idx, probe_idx, lig2_idx).to_degrees();
                            hist.insert(angle);
                        });
                });
        });
    }

    hist
}
