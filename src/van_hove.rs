use crate::util::Array3Ops;
use chemfiles as chfl;
use chfl_tools::centers::{Center, WeightType};
use chfl_tools::datasets::Histogram;
use chfl_tools::ranges::LogRange;
use log::{debug, info};
use rayon::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

const LOG_STEPS: usize = 1000;

fn create_position_ts(
    traj: &mut chfl::Trajectory,
    sel: &[usize],
    step_size: Option<usize>,
    rmcomm: bool,
) -> Vec<ndarray::Array2<f64>> {
    let step_size = step_size.unwrap_or(1);
    let sel_hs: HashSet<_> = HashSet::from_iter(sel);

    info!("Store positions...");
    let mut positions = Vec::new();
    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..traj.nsteps().unwrap()).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let com = if rmcomm {
            frame.center(Some(WeightType::Mass), false)
        } else {
            [0.0; 3]
        };
        #[allow(clippy::filter_map)]
        let sel_positions: Vec<[f64; 3]> = frame
            .positions()
            .par_iter()
            .enumerate()
            .filter(|(i, _)| sel_hs.contains(i))
            .map(|(_, &pos)| if rmcomm { pos.neg(&com) } else { pos })
            .collect();
        positions.push(ndarray::arr2(&sel_positions));
    }
    positions
}

pub fn vanhove_self(
    traj: &mut chfl::Trajectory,
    sel: &[usize],
    n_bins: usize,
    r_max: f64,
    step_size: Option<usize>,
    rmcomm: bool,
) -> Vec<(usize, Histogram<f64>)> {
    let positions = create_position_ts(traj, sel, step_size, rmcomm);
    let n_particles = sel.len();

    LogRange::new(1, positions.len() - 1, 2, true)
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(|step| {
            info!("step: {}", step);
            let mut hist = Histogram::<f64>::new(n_bins, 0.0, r_max);
            let n_steps = positions.len() - step;
            #[allow(clippy::cast_precision_loss)]
            let weight = 1.0 / (n_steps * n_particles) as f64;
            (0..n_steps).for_each(|i| {
                let j = i + step;
                let disp = &positions[j] - &positions[i];
                let disp_arr: Vec<_> = disp
                    .outer_iter()
                    .map(|dv| dv.dot(&dv).sqrt())
                    .filter(|&d| d < r_max)
                    .collect();
                #[allow(clippy::cast_precision_loss)]
                disp_arr
                    .into_iter()
                    .for_each(|d| hist.insert_weighted(d, weight));
            });
            (step, hist)
        })
        .collect()
}

pub fn vanhove_distinct(
    traj: &mut chfl::Trajectory,
    sel: &[usize],
    n_bins: usize,
    r_max: f64,
    step_size: Option<usize>,
    rmcomm: bool,
) -> Vec<(usize, Histogram<f64>)> {
    let positions = create_position_ts(traj, sel, step_size, rmcomm);
    let n_particles = sel.len();

    LogRange::new(1, positions.len() - 1, 2, true)
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(|step| {
            info!("step: {}", step);
            let mut histogram = Histogram::<f64>::new(n_bins, 0.0, r_max);
            let n_steps = positions.len() - step;
            #[allow(clippy::cast_precision_loss)]
            let weight = 2.0 / (n_steps * n_particles) as f64;
            (0..n_steps).for_each(|i| {
                let j = i + step;
                let dist: Vec<_> = (0..n_particles)
                    .into_par_iter()
                    .map(|a| {
                        ((a + 1)..n_particles)
                            .map(|b| {
                                let dv = &positions[j].slice(ndarray::s![b, 0..])
                                    - &positions[i].slice(ndarray::s![a, 0..]);
                                dv.dot(&dv).sqrt()
                            })
                            .collect::<Vec<_>>()
                    })
                    .flatten()
                    .filter(|&d| d < r_max)
                    .collect();
                dist.into_iter()
                    .for_each(|d| histogram.insert_weighted(d, weight))
            });
            (step, histogram)
        })
        .collect()
}
