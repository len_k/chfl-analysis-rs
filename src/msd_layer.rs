use crate::util::{Array3Ops, Direction};
use chemfiles as chfl;
use chfl_tools::centers::{Center, WeightType};
use chfl_tools::datasets::WOADataset;
use chfl_tools::ranges::LogRange;
use log::{debug, info};
use rayon::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

type StepResult = (usize, Vec<(f64, f64, usize)>);

const LOG_STEPS: usize = 1000;

pub fn msd_layer(
    traj: &mut chfl::Trajectory,
    sel: &[usize],
    n_bins: usize,
    step_size: Option<usize>,
    normal: Direction,
    lateral: Option<Direction>,
    rmcomm: bool,
) -> Vec<StepResult> {
    let step_size = step_size.unwrap_or(1);
    let n_steps = traj.nsteps().unwrap();
    let sel_hs: HashSet<_> = HashSet::from_iter(sel);

    info!("Store positions...");
    let mut positions = Vec::new();
    let mut bins = Vec::new();
    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..n_steps).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();
        assert_eq!(
            frame.cell().shape(),
            chfl::CellShape::Orthorhombic,
            "Cell of step {} must be orthorhombic",
            timestep
        );
        let com = if rmcomm {
            frame.center(Some(WeightType::Mass), false)
        } else {
            [0.0; 3]
        };
        #[allow(clippy::filter_map)]
        let mut sel_positions: Vec<[f64; 3]> = frame
            .positions()
            .iter()
            .enumerate()
            .filter(|(i, _)| sel_hs.contains(i))
            .map(|(_, &pos)| if rmcomm { pos.neg(&com) } else { pos })
            .collect();
        let height = frame.cell().lengths()[normal as usize];
        #[allow(
            clippy::cast_possible_truncation,
            clippy::cast_precision_loss,
            clippy::cast_sign_loss
        )]
        let frame_bins: Vec<_> = sel_positions
            .iter()
            .map(|pos| (((pos[normal as usize] / height) % 1.0) * n_bins as f64).floor())
            .map(|bin| {
                if bin < 0.0 {
                    (bin + n_bins as f64) as usize
                } else {
                    bin as usize
                }
            })
            .collect();
        if let Some(lateral) = lateral {
            sel_positions
                .iter_mut()
                .for_each(|pos| pos[lateral as usize] = 0.0);
        }
        let frame_positions = ndarray::arr2(&sel_positions);
        assert_eq!(frame_positions.len(), 3 * frame_bins.len());
        bins.push(frame_bins);
        positions.push(frame_positions);
    }
    assert_eq!(positions.len(), bins.len());

    LogRange::new(1, positions.len() - 1, 2, true)
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(|step| {
            info!("step: {}", step);
            let mut step_data = vec![WOADataset::new(); n_bins];
            for i in 0..(positions.len() - step) {
                let j = i + step;
                let disp = &positions[j] - &positions[i];
                let squared_disp: Vec<_> = disp.outer_iter().map(|dv| dv.dot(&dv)).collect();
                squared_disp
                    .iter()
                    .zip(bins[i].iter())
                    .for_each(|(&sd, &bin)| step_data[bin].update(sd));
            }
            (
                step,
                step_data
                    .iter()
                    .map(|sd| {
                        (
                            sd.calc_mean().unwrap_or(0.0),
                            sd.calc_std().unwrap_or(0.0),
                            sd.len(),
                        )
                    })
                    .collect(),
            )
        })
        .collect()
}
