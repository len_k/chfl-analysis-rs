use chemfiles as chfl;
use log::{debug, info};
use ndarray::{s, Array1};

const LOG_STEPS: usize = 1000;

fn rdf_frame(
    frame: &chfl::Frame,
    sel_a: &[usize],
    sel_b: &[usize],
    nbins: usize,
    rmax: f64,
) -> (Array1<f64>, f64) {
    let mut count = Array1::<f64>::zeros(nbins);
    let nsgrid = nsgrid::NSGrid::with_selection(frame, rmax as f64, Vec::from(sel_b)).unwrap();
    let positions = nsgrid.frame().positions();
    sel_a.iter().for_each(|idx_a| {
        let nsresult = nsgrid.get_position_neighbours(&positions[*idx_a]).unwrap();
        nsresult.iter().for_each(|idx_b| {
            let dist = nsgrid.frame().distance_sq(*idx_a, *idx_b);
            if dist < rmax.powi(2) {
                // NSGrid searches for less or equal
                #[allow(
                    clippy::cast_possible_truncation,
                    clippy::cast_precision_loss,
                    clippy::cast_sign_loss
                )]
                let bin = (dist.sqrt() / rmax * nbins as f64).floor() as usize;
                count[bin] += 1.0;
            }
        });
    });
    (count, nsgrid.frame().cell().volume())
}

pub fn rdf(
    traj: &mut chfl::Trajectory,
    sel_a: &[usize],
    sel_b: &[usize],
    n_bins: usize,
    r_max: f64,
    step_size: Option<usize>,
) -> Vec<[f64; 2]> {
    let step_size = step_size.unwrap_or(1);
    let nsteps = traj.nsteps().unwrap();

    let mut count = Array1::<f64>::zeros(n_bins);
    let mut volume: f64 = 0.0;

    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..nsteps).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(step, &mut frame).unwrap();
        let (frame_count, frame_volume) = rdf_frame(&frame, &sel_a, &sel_b, n_bins, r_max);
        count += &frame_count;
        volume += frame_volume;
    }

    #[allow(clippy::cast_precision_loss)]
    let dr: f64 = r_max / n_bins as f64;
    #[allow(clippy::cast_precision_loss)]
    let bins_r = Array1::<f64>::from_shape_fn(n_bins, |i| i as f64 * dr + dr / 2.0);

    #[allow(clippy::cast_precision_loss)]
    let edges = Array1::<f64>::from_shape_fn(n_bins + 1, |i| i as f64 * dr);
    let mut r_shell_volume = edges.slice(s![1..]).to_owned();
    r_shell_volume.map_inplace(|i| *i = i.powf(3.0));
    let mut l_shell_volume = edges.slice(s![..-1]).to_owned();
    l_shell_volume.map_inplace(|i| *i = i.powf(3.0));
    let shell_volume = (r_shell_volume - l_shell_volume) * 4.0 / 3.0 * std::f64::consts::PI;

    let num_pairs = sel_a.len() * sel_b.len();
    #[allow(clippy::cast_precision_loss)]
    let density = num_pairs as f64 / (volume / nsteps as f64);

    #[allow(clippy::cast_precision_loss)]
    let rdf = count / (density * shell_volume * nsteps as f64);

    bins_r
        .into_iter()
        .zip(rdf.into_iter())
        .map(|(bin_r, rdf_val)| [*bin_r, *rdf_val])
        .collect()
}
