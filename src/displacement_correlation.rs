use crate::util::Array3Ops;
use chemfiles as chfl;
use chfl_tools::centers::{Center, WeightType};
use chfl_tools::datasets::WOADataset;
use chfl_tools::ranges::LogRange;
use log::{debug, info};
use rayon::prelude::*;
use std::collections::HashSet;
use std::iter::FromIterator;

type StepResult = (usize, [(f64, f64, usize); 5]);

const LOG_STEPS: usize = 1000;

fn add_positions(
    positions: &mut Vec<ndarray::Array2<f64>>,
    frame: &chfl::Frame,
    sel_hs: &HashSet<&usize>,
    com: Option<[f64; 3]>,
) {
    #[allow(clippy::filter_map)]
    let sel_positions: Vec<[f64; 3]> = frame
        .positions()
        .par_iter()
        .enumerate()
        .filter(|(i, _)| sel_hs.contains(i))
        .map(|(_, &pos)| {
            if let Some(com) = com {
                pos.neg(&com)
            } else {
                pos
            }
        })
        .collect();
    let frame_positions = ndarray::arr2(&sel_positions);
    positions.push(frame_positions);
}

struct StepData {
    daa_self: Vec<WOADataset>,
    daa_dist: Vec<WOADataset>,
    dbb_self: Vec<WOADataset>,
    dbb_dist: Vec<WOADataset>,
    dab: Vec<WOADataset>,
}

impl StepData {
    fn new(num_a: usize, num_b: usize) -> Self {
        Self {
            daa_self: vec![WOADataset::new(); num_a],
            daa_dist: vec![WOADataset::new(); num_a.pow(2)],
            dbb_self: vec![WOADataset::new(); num_b],
            dbb_dist: vec![WOADataset::new(); num_b.pow(2)],
            dab: vec![WOADataset::new(); num_a * num_b],
        }
    }

    fn calc_result(&self) -> [(f64, f64, usize); 5] {
        [
            (
                self.daa_self
                    .iter()
                    .map(|ds| ds.calc_mean().unwrap_or(0.0))
                    .sum(),
                self.daa_self
                    .iter()
                    .map(|ds| ds.calc_std().unwrap_or(0.0))
                    .sum(),
                self.daa_self.iter().map(WOADataset::len).sum(),
            ),
            (
                self.daa_dist
                    .iter()
                    .map(|ds| ds.calc_mean().unwrap_or(0.0))
                    .sum::<f64>()
                    * 2.0,
                self.daa_dist
                    .iter()
                    .map(|ds| ds.calc_std().unwrap_or(0.0))
                    .sum::<f64>()
                    * 2.0,
                self.daa_dist.iter().map(WOADataset::len).sum::<usize>() * 2,
            ),
            (
                self.dbb_self
                    .iter()
                    .map(|ds| ds.calc_mean().unwrap_or(0.0))
                    .sum(),
                self.dbb_self
                    .iter()
                    .map(|ds| ds.calc_std().unwrap_or(0.0))
                    .sum(),
                self.dbb_self.iter().map(WOADataset::len).sum(),
            ),
            (
                self.dbb_dist
                    .iter()
                    .map(|ds| ds.calc_mean().unwrap_or(0.0))
                    .sum::<f64>()
                    * 2.0,
                self.dbb_dist
                    .iter()
                    .map(|ds| ds.calc_std().unwrap_or(0.0))
                    .sum::<f64>()
                    * 2.0,
                self.dbb_dist.iter().map(WOADataset::len).sum::<usize>() * 2,
            ),
            (
                self.dab
                    .iter()
                    .map(|ds| ds.calc_mean().unwrap_or(0.0))
                    .sum(),
                self.dab.iter().map(|ds| ds.calc_std().unwrap_or(0.0)).sum(),
                self.dab.iter().map(WOADataset::len).sum(),
            ),
        ]
    }
}

fn same_type_self(disp: &ndarray::Array2<f64>, datasets: &mut [WOADataset]) {
    disp.outer_iter()
        .enumerate()
        .map(|(n, d)| (n, d.dot(&d)))
        .for_each(|(n, sd)| datasets[n].update(sd));
}

fn same_type_dist(disp: &ndarray::Array2<f64>, datasets: &mut [WOADataset], num: usize) {
    disp.outer_iter().enumerate().for_each(|(m, dm)| {
        disp.outer_iter()
            .enumerate()
            .skip(m + 1)
            .for_each(|(n, dn)| {
                datasets[m * num + n].update(dm.dot(&dn));
            });
    });
}

fn diff_type(
    disp_a: &ndarray::Array2<f64>,
    disp_b: &ndarray::Array2<f64>,
    datasets: &mut [WOADataset],
    num_b: usize,
) {
    disp_a.outer_iter().enumerate().for_each(|(m, dm)| {
        disp_b.outer_iter().enumerate().for_each(|(n, dn)| {
            datasets[m * num_b + n].update(dm.dot(&dn));
        });
    });
}

pub fn disp_corr(
    traj: &mut chfl::Trajectory,
    sel_a: &[usize],
    sel_b: &[usize],
    step_size: Option<usize>,
    rmcomm: bool,
) -> Vec<StepResult> {
    let step_size = step_size.unwrap_or(1);
    let n_steps = traj.nsteps().unwrap();
    let sel_hs_a: HashSet<_> = HashSet::from_iter(sel_a);
    let sel_hs_b: HashSet<_> = HashSet::from_iter(sel_b);
    let num_a = sel_a.len();
    let num_b = sel_b.len();

    info!("Store positions...");
    let mut positions_a = Vec::new();
    let mut positions_b = Vec::new();
    let mut frame = chfl::Frame::new();
    for (step, timestep) in (0..n_steps).step_by(step_size).enumerate() {
        if step % LOG_STEPS == 0 {
            info!("timestep: {}", timestep);
        } else {
            debug!("timestep: {}", timestep);
        }
        traj.read_step(timestep, &mut frame).unwrap();

        let com = if rmcomm {
            Some(frame.center(Some(WeightType::Mass), false))
        } else {
            None
        };
        add_positions(&mut positions_a, &frame, &sel_hs_a, com);
        add_positions(&mut positions_b, &frame, &sel_hs_b, com);
    }

    assert_eq!(positions_a.len(), positions_b.len());
    LogRange::new(1, positions_a.len() - 1, 2, true)
        .unwrap()
        .collect::<Vec<_>>()
        .into_par_iter()
        .map(|step| {
            info!("step: {}", step);
            let mut step_data = StepData::new(num_a, num_b);
            for i in 0..(positions_a.len() - step) {
                let j = i + step;
                let disp_a = &positions_a[j] - &positions_a[i];
                same_type_self(&disp_a, &mut step_data.daa_self);
                same_type_dist(&disp_a, &mut step_data.daa_dist, num_a);
                let disp_b = &positions_b[j] - &positions_b[i];
                same_type_self(&disp_b, &mut step_data.dbb_self);
                same_type_dist(&disp_b, &mut step_data.dbb_dist, num_a);
                diff_type(&disp_a, &disp_b, &mut step_data.dab, num_b);
            }
            (step, step_data.calc_result())
        })
        .collect()
}
