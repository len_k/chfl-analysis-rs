extern crate chfl_analysis;
extern crate chfl_tools;
use crate::chfl_tools::filters::*;
use chemfiles as chfl;
use log::info;
use simplelog::*;
use std::path::Path;

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let atoms_thf = chfl::Selection::new("resname(#1) THF")
        .unwrap()
        .list(&frame)
        .unwrap();
    let res_thf = frame.unique_residues(atoms_thf);

    let topol = frame.topology();
    let atom_groups: Vec<_> = res_thf
        .into_iter()
        .map(|res_idx| {
            let res = topol
                .residue(res_idx - 1)
                .expect("Residue index not found.");
            assert_eq!(res.id().unwrap(), res_idx);
            let c_idx: Vec<_> = res
                .atoms()
                .into_iter()
                .filter(|&i| frame.atom(i).name() == "C")
                .collect();
            assert_eq!(c_idx.len(), 4);
            let o_idx: Vec<_> = res
                .atoms()
                .into_iter()
                .filter(|&i| frame.atom(i).name() == "O")
                .collect();
            assert_eq!(o_idx.len(), 1);
            [c_idx, o_idx]
        })
        .collect();

    info!("Calculating orientation for residues...");
    let result = chfl_analysis::orient_profile(&mut traj, &atom_groups, 100, None);
    info!("Done");
    result
        .to_csv(path.join("orientation_profile.csv").to_str().unwrap())
        .unwrap();
}
