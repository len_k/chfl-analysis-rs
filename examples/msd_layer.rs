extern crate chfl_analysis;
use chemfiles as chfl;
use csv::Writer;
use log::info;
use simplelog::*;
use std::path::Path;

fn write_result(
    path: &str,
    msd_result: &[(usize, Vec<(f64, f64, usize)>)],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;
    let mut header = vec!["steps".to_string()];
    if !msd_result.is_empty() {
        msd_result[0].1.iter().enumerate().for_each(|(i, _)| {
            header.push(format!("mean_{}", i));
            header.push(format!("std_{}", i));
            header.push(format!("count_{}", i));
        });
    }
    wtr.write_record(&header)?;
    for ts in msd_result {
        let mut line = vec![format!("{}", ts.0)];
        for entry in ts.1.iter() {
            line.push(format!("{}", entry.0));
            line.push(format!("{}", entry.1));
            line.push(format!("{}", entry.2));
        }
        wtr.write_record(&line)?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();
    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();

    let result = chfl_analysis::msd_layer(
        &mut traj,
        &sel_li,
        6,
        None,
        chfl_analysis::util::Direction::Z,
        Some(chfl_analysis::util::Direction::Z),
        false,
    );

    write_result(path.join("msd_layer.csv").to_str().unwrap(), &result).unwrap();
}
