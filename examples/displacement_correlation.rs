extern crate chfl_analysis;
extern crate chfl_tools;
use crate::chfl_tools::filters::*;
use chemfiles as chfl;
use csv::Writer;
use log::info;
use regex::Regex;
use simplelog::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::path::Path;

fn write_result(
    path: &str,
    dc_result: &[(usize, [(f64, f64, usize); 5])],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;
    wtr.write_record(&[
        "steps",
        "mean_daa_self",
        "std_daa_self",
        "count_daa_self",
        "mean_daa_dist",
        "std_daa_dist",
        "count_daa_dist",
        "mean_dbb_self",
        "std_dbb_self",
        "count_dbb_self",
        "mean_dbb_dist",
        "std_dbb_dist",
        "count_dbb_dist",
        "mean_dab",
        "std_dab",
        "count_dab",
    ])?;
    for entry in dc_result {
        let mut record = vec![format!("{}", entry.0)];
        for part in &entry.1 {
            record.push(format!("{}", part.0));
            record.push(format!("{}", part.1));
            record.push(format!("{}", part.2));
        }
        wtr.write_record(&record)?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let re_nitro = Regex::new(r"^N.*$").unwrap();

    let sel_nitro: HashSet<_> = HashSet::from_iter(frame.filter_name(&re_nitro));

    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();
    let sel_tfsi: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) TFSI")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );

    let sel_tfsi_n: Vec<_> = sel_tfsi.intersection(&sel_nitro).copied().collect();

    info!("Calculating displacement correlation...");
    let result = chfl_analysis::disp_corr(&mut traj, &sel_li, &sel_tfsi_n, None, false);
    info!("Done");

    write_result(path.join("disp_corr.csv").to_str().unwrap(), &result).unwrap();
}
