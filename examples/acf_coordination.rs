extern crate chfl_analysis;
extern crate chfl_tools;
use crate::chfl_tools::filters::*;
use chemfiles as chfl;
use csv::Writer;
use log::info;
use regex::Regex;
use simplelog::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::path::Path;

fn write_result(
    path: &str,
    lig_result: &[(usize, f64, f64, usize)],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;
    wtr.write_record(&["steps", "mean", "std", "count"])?;
    for entry in lig_result {
        wtr.write_record(&[
            format!("{}", entry.0),
            format!("{}", entry.1),
            format!("{}", entry.2),
            format!("{}", entry.3),
        ])?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let re_oxy = Regex::new(r"^O.*$").unwrap();

    let sel_oxy: HashSet<_> = HashSet::from_iter(frame.filter_name(&re_oxy));

    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();
    let sel_pol: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) POL")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_tfsi: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) TFSI")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_thf: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) THF")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );

    let sel_pol_o: Vec<_> = sel_pol.intersection(&sel_oxy).copied().collect();
    let sel_tfsi_o: Vec<_> = sel_tfsi.intersection(&sel_oxy).copied().collect();
    let sel_thf_o: Vec<_> = sel_thf.intersection(&sel_oxy).copied().collect();

    info!("Calculating coordination ACF for ligand atoms...");
    let result = chfl_analysis::acf_coord(
        &mut traj,
        &sel_li,
        &[&sel_pol_o, &sel_tfsi_o, &sel_thf_o],
        2.8,
        None,
        false,
    );
    info!("Done");
    write_result(path.join("acf_pol_o.csv").to_str().unwrap(), &result[0]).unwrap();
    write_result(path.join("acf_tfsi_o.csv").to_str().unwrap(), &result[1]).unwrap();
    write_result(path.join("acf_thf_o.csv").to_str().unwrap(), &result[2]).unwrap();

    info!("Calculating coordination ACF for ligand residues...");
    let result_res = chfl_analysis::acf_coord(
        &mut traj,
        &sel_li,
        &[&sel_pol_o, &sel_tfsi_o],
        2.8,
        None,
        true,
    );
    info!("Done");
    write_result(
        path.join("acf_pol_res.csv").to_str().unwrap(),
        &result_res[0],
    )
    .unwrap();
    write_result(
        path.join("acf_tfsi_res.csv").to_str().unwrap(),
        &result_res[1],
    )
    .unwrap();
}
