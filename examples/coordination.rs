extern crate chfl_analysis;
extern crate chfl_tools;
use crate::chfl_tools::filters::*;
use chemfiles as chfl;
use csv::Writer;
use log::info;
use regex::Regex;
use simplelog::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::path::Path;

fn write_result(
    path: &str,
    coord_result: &[(usize, f64, f64, f64)],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;
    let header = vec!["", "mean", "std", "frequency"];
    wtr.write_record(&header)?;
    for cn_data in coord_result {
        let line = [
            format!("{}", cn_data.0),
            format!("{}", cn_data.1),
            format!("{}", cn_data.2),
            format!("{}", cn_data.3),
        ];
        wtr.write_record(&line)?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let re_oxy = Regex::new(r"^O.*$").unwrap();

    let sel_oxy: HashSet<_> = HashSet::from_iter(frame.filter_name(&re_oxy));

    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();
    let sel_pol: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) POL")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_tfsi: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) TFSI")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_thf: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) THF")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );

    let sel_pol_o: Vec<_> = sel_pol.intersection(&sel_oxy).copied().collect();
    let sel_tfsi_o: Vec<_> = sel_tfsi.intersection(&sel_oxy).copied().collect();
    let sel_thf_o: Vec<_> = sel_thf.intersection(&sel_oxy).copied().collect();
    let sel_ether_o = {
        let mut sel_ether_o = sel_pol_o.clone();
        sel_ether_o.extend(&sel_thf_o);
        sel_ether_o
    };
    let sel_all_o = {
        let mut sel_all_o = sel_pol_o.clone();
        sel_all_o.extend(&sel_tfsi_o);
        sel_all_o.extend(&sel_thf_o);
        sel_all_o
    };

    info!("Calculating coordinations Li-O ...");
    let result = chfl_analysis::coord(
        &mut traj,
        &sel_li,
        &[
            &sel_pol_o,
            &sel_tfsi_o,
            &sel_thf_o,
            &sel_ether_o,
            &sel_all_o,
        ],
        2.8,
        None,
        false,
    );
    info!("Done");
    write_result(
        path.join("coord_li_pol_o.csv").to_str().unwrap(),
        &result[0],
    )
    .unwrap();
    write_result(
        path.join("coord_li_tfsi_o.csv").to_str().unwrap(),
        &result[1],
    )
    .unwrap();
    write_result(
        path.join("coord_li_thf_o.csv").to_str().unwrap(),
        &result[2],
    )
    .unwrap();
    write_result(
        path.join("coord_li_ether_o.csv").to_str().unwrap(),
        &result[3],
    )
    .unwrap();
    write_result(
        path.join("coord_li_all_o.csv").to_str().unwrap(),
        &result[4],
    )
    .unwrap();

    info!("Calculating coordinations Li-Res ...");
    let result_res = chfl_analysis::coord(
        &mut traj,
        &sel_li,
        &[&sel_pol_o, &sel_tfsi_o],
        2.8,
        None,
        true,
    );
    info!("Done");
    write_result(
        path.join("coord_li_pol_res.csv").to_str().unwrap(),
        &result_res[0],
    )
    .unwrap();
    write_result(
        path.join("coord_li_tfsi_res.csv").to_str().unwrap(),
        &result_res[1],
    )
    .unwrap();

    info!("Calculating coordinations Res-Li ...");
    let result_pol = chfl_analysis::coord_res(&mut traj, &sel_pol_o, &[&sel_li], 2.8, None, false);
    let result_tfsi =
        chfl_analysis::coord_res(&mut traj, &sel_tfsi_o, &[&sel_li], 2.8, None, false);
    let result_thf = chfl_analysis::coord_res(&mut traj, &sel_thf_o, &[&sel_li], 2.8, None, false);
    info!("Done");
    write_result(
        path.join("coord_res_pol_li.csv").to_str().unwrap(),
        &result_pol[0],
    )
    .unwrap();
    write_result(
        path.join("coord_res_tfsi_li.csv").to_str().unwrap(),
        &result_tfsi[0],
    )
    .unwrap();
    write_result(
        path.join("coord_res_thf_li.csv").to_str().unwrap(),
        &result_thf[0],
    )
    .unwrap();
}
