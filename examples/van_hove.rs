extern crate chfl_analysis;
use chemfiles as chfl;
use chfl_tools::datasets::Histogram;
use csv::Writer;
use log::info;
use simplelog::*;
use std::path::Path;

fn write_result(
    path: &str,
    hist_result: &[(usize, Histogram<f64>)],
) -> Result<(), Box<dyn std::error::Error>> {
    if hist_result.is_empty() {
        return Ok(());
    }
    let n_bins = hist_result[0].1.n_bins();
    let mut wtr = Writer::from_path(path)?;
    let mut header = vec![String::new()];
    header.extend(
        (0..n_bins)
            .map(|b| format!("{}", hist_result[0].1.value(b)))
            .collect::<Vec<_>>(),
    );
    wtr.write_record(&header)?;
    for ts in hist_result {
        let mut line = vec![format!("{}", ts.0)];
        line.extend(
            ts.1.data()
                .into_iter()
                .map(|val| format!("{}", val))
                .collect::<Vec<_>>(),
        );
        wtr.write_record(&line)?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();
    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();

    info!("Calculating self van Hove...");
    let result = chfl_analysis::vanhove_self(&mut traj, &sel_li, 150, 30.0, None, false);
    info!("Done");
    write_result(path.join("vanhove_self.csv").to_str().unwrap(), &result).unwrap();

    info!("Calculating distinct van Hove...");
    let result = chfl_analysis::vanhove_distinct(&mut traj, &sel_li, 150, 30.0, None, false);
    info!("Done");
    write_result(path.join("vanhove_distinct.csv").to_str().unwrap(), &result).unwrap();
}
