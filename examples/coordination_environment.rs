extern crate chfl_analysis;
extern crate chfl_tools;
use crate::chfl_tools::filters::*;
use chemfiles as chfl;
use csv::Writer;
use log::info;
use regex::Regex;
use simplelog::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::path::Path;

fn write_result(
    path: &str,
    env_result: &[(usize, Vec<usize>)],
) -> Result<(), Box<dyn std::error::Error>> {
    let mut wtr = Writer::from_path(path)?;
    let mut header = vec!["count", "n_pol", "n_tfsi", "n_thf"];
    if !env_result.is_empty() {
        if env_result[0].1.len() > 3 {
            header.extend(&["n_pol_res", "n_tfsi_res", "n_thf_res"]);
        }
    }
    wtr.write_record(&header)?;
    for ts in env_result {
        let mut line = vec![format!("{}", ts.0)];
        for cn in ts.1.iter() {
            line.push(format!("{}", cn));
        }
        wtr.write_record(&line)?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();

    let re_oxy = Regex::new(r"^O.*$").unwrap();

    let sel_oxy: HashSet<_> = HashSet::from_iter(frame.filter_name(&re_oxy));

    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();
    let sel_pol: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) POL")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_tfsi: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) TFSI")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_thf: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) THF")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );

    let sel_pol_o: Vec<_> = sel_pol.intersection(&sel_oxy).copied().collect();
    let sel_tfsi_o: Vec<_> = sel_tfsi.intersection(&sel_oxy).copied().collect();
    let sel_thf_o: Vec<_> = sel_thf.intersection(&sel_oxy).copied().collect();

    info!("Calculating coordination environment...");
    let result = chfl_analysis::coord_env(
        &mut traj,
        &sel_li,
        &[&sel_pol_o, &sel_tfsi_o, &sel_thf_o],
        2.8,
        None,
        true,
    );
    info!("Done");
    write_result(path.join("coord_env.csv").to_str().unwrap(), &result).unwrap();
}
