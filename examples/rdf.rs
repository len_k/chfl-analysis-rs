extern crate chfl_analysis;
use chemfiles as chfl;
use chfl_tools::filters::*;
use csv::Writer;
use log::info;
use regex::Regex;
use simplelog::*;
use std::collections::HashSet;
use std::iter::FromIterator;
use std::path::Path;

fn write_result(path: &str, rdf_result: &[[f64; 2]]) -> Result<(), Box<dyn std::error::Error>> {
    if rdf_result.is_empty() {
        return Ok(());
    }
    let mut wtr = Writer::from_path(path)?;
    wtr.write_record(&["bin".to_string(), "rdf".to_string()])?;
    for val in rdf_result {
        wtr.write_record(&[format!("{}", val[0]), format!("{}", val[1])])?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    TermLogger::init(
        LevelFilter::Info,
        chfl_analysis::util::log_format_date_time(),
        TerminalMode::Mixed,
    )
    .expect("No interactive terminal");
    info!("Chemfiles version: {}", chfl::version());

    let args = chfl_analysis::util::get_cli_args();
    let traj_path = args.0.unwrap();
    let mut traj = chfl::Trajectory::open(&traj_path, 'r').unwrap();
    info!(
        "Found {} steps in trajectory '{}'",
        traj.nsteps().unwrap(),
        traj_path
    );

    if let Some(topol_path) = args.1 {
        let mut topol_traj = chfl::Trajectory::open(&topol_path, 'r').unwrap();
        let mut frame = chfl::Frame::new();
        topol_traj.read(&mut frame).unwrap();
        traj.set_topology(&frame.topology());
        info!("Using first step of '{}' as topology", topol_path);
    };

    let out_path_str = args.2.unwrap_or(".".to_string());
    let path = Path::new(&out_path_str);
    if !path.is_dir() {
        panic!("Output path must be an existing directory");
    }
    info!(
        "Output path: '{}'",
        path.canonicalize().unwrap().to_str().unwrap()
    );

    let mut frame = chfl::Frame::new();
    traj.read_step(0, &mut frame).unwrap();
    let sel_li = chfl::Selection::new("resname(#1) LI")
        .unwrap()
        .list(&frame)
        .unwrap();
    let re_oxy = Regex::new(r"^O.*$").unwrap();
    let sel_oxy: HashSet<_> = HashSet::from_iter(frame.filter_name(&re_oxy));
    let sel_tfsi: HashSet<_> = HashSet::from_iter(
        chfl::Selection::new("resname(#1) TFSI")
            .unwrap()
            .list(&frame)
            .unwrap(),
    );
    let sel_tfsi_o: Vec<_> = sel_tfsi.intersection(&sel_oxy).copied().collect();

    info!("Calculating RDF...");
    let result = chfl_analysis::rdf(&mut traj, &sel_li, &sel_tfsi_o, 750, 15.0, None);
    info!("Done");
    write_result(path.join("rdf.csv").to_str().unwrap(), &result).unwrap();
}
